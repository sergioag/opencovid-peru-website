from django.urls import path

from .views import find_report_dynamically_view

urlpatterns = [    
    # this is meant to be a catch all to dynamically pull report
    path('<slug:the_slug>/', find_report_dynamically_view, name='find_report'),
]